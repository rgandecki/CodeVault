/*includes {{{*/
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <mkl.h>
#define MIC_TARGET __declspec(target(mic))
#include "../../common/mmio.h"
#include "../../common/util.h"
#include "../../common/csr.h"
/*}}}*/

/* global varibles and defines {{{*/
int micdev = 0;
__declspec(target(mic)) int nrepeat = 100;
__declspec(target(mic)) char transa = 'n';
__declspec(target(mic)) int nthreads;
/*}}}*/

/** Multiply two sparse matrices which are stored in CSR format. MKL is used */
void mkl_mic_spgemm(MKL_INT Am, MKL_INT An, MKL_INT Annz, double* Aval, MKL_INT* AJ, MKL_INT* AI, MKL_INT Bn, MKL_INT Bnnz, double* Bval, MKL_INT* BJ, MKL_INT* BI, double** pCval, MKL_INT** pCJ, MKL_INT** pCI, double* time) {/*{{{*/
    int i;
    #pragma offload target(mic:micdev) inout(nthreads)
    {
        if (nthreads > 0) {
            mkl_set_num_threads(nthreads);
        } else {
            nthreads = mkl_get_max_threads();
        }
    }
    #pragma offload target(mic:micdev) \
        in(transa) \
        in(Am) \
        in(An) \
        in(Bn) \
        in(Aval:length(Annz)    free_if(0)) \
        in(AI:length(Am+1)    free_if(0)) \
        in(AJ:length(Annz)    free_if(0)) \
        in(Bval:length(Bnnz)    free_if(0)) \
        in(BI:length(An+1)    free_if(0)) \
        in(BJ:length(Bnnz)    free_if(0))
    {}

    MKL_INT    Cnnz_host = -1;
    MKL_INT* CI = NULL;
    MKL_INT* CJ = NULL;
    double* Cval = NULL;
    MKL_INT nnzmax = 0;    // nnzmax is zero in case of symbolic&numeric usage of mkl_?csrmultcsr
    MKL_INT ierr;
    MKL_INT request = 2;    //numeric multiplication
    MKL_INT sort = 3;    // sort everything
    double time_mic_symbolic_mm = 0.0;
    #pragma offload target(mic:micdev) in(i) in(ierr) in(nnzmax) in(request) in(sort) in(transa) in(Am)  in(An) in(Bn) \
        out(time_mic_symbolic_mm) out(Cnnz_host)\
        in(Aval:length(Annz)    alloc_if(0)    free_if(0)) \
        in(AI:length(Am+1)    alloc_if(0)    free_if(0)) \
        in(AJ:length(Annz)    alloc_if(0)    free_if(0)) \
        in(Bval:length(Bnnz)    alloc_if(0)    free_if(0)) \
        in(BI:length(An+1)    alloc_if(0)    free_if(0)) \
        in(BJ:length(Bnnz)    alloc_if(0)    free_if(0)) \
        nocopy(CI:    alloc_if(0)    free_if(0)) \
        nocopy(CJ:    alloc_if(0)    free_if(0)) \
        nocopy(Cval:    alloc_if(0)    free_if(0))
    {
        CI = (MKL_INT*)mkl_malloc( (Am+2) * sizeof( MKL_INT ), 64 );
        MKL_INT nnzmax = 0;    // nnzmax is zero in case of symbolic&numeric usage of mkl_?csrmultcsr
        MKL_INT ierr;
        MKL_INT request = 1;    // symbolic multiplication

        for(i = 0; i < 10; i++) {
            mkl_dcsrmultcsr(&transa, &request, &sort, &Am, &An, &Bn, Aval, AJ, AI, Bval, BJ, BI, Cval, CJ, CI, &nnzmax, &ierr);
        }
        double s_initial = dsecnd();
        for(i = 0; i < nrepeat; i++) {
            mkl_dcsrmultcsr(&transa, &request, &sort, &Am, &An, &Bn, Aval, AJ, AI, Bval, BJ, BI, Cval, CJ, CI, &nnzmax, &ierr);
        }
        time_mic_symbolic_mm = (dsecnd() - s_initial) / nrepeat; // seconds

        request = 2;        //numeric multiplication
        int Cnnz = CI[Am]-1;
        int Cval_size = Cnnz + 1; Cnnz_host = Cnnz;
        CJ        = (MKL_INT*)mkl_malloc( Cval_size    *    sizeof( MKL_INT ), 64 );
        Cval        = (double*)mkl_malloc( Cval_size    *    sizeof( double ), 64 );
        mkl_dcsrmultcsr(&transa, &request, &sort, &Am, &An, &An, Aval, AJ, AI, Bval, BJ, BI, Cval, CJ, CI, &nnzmax, &ierr);
        printmm_one(Am, Cval, CJ, CI);
        sort = 7;        // do not sort anything
        request = 2;        // numeric multiplication
    }
    double time_mic_numeric_mm = 0.0;
    #pragma offload target(mic:micdev) nocopy(request) nocopy(sort) nocopy(i) nocopy(ierr) nocopy(nnzmax)  nocopy(transa) nocopy(Am) nocopy(An) nocopy(Bn) \
        out(time_mic_numeric_mm) \
        nocopy(Aval:length(Annz)    alloc_if(0)    free_if(0)) \
        nocopy(AI:length(Am+1)    alloc_if(0)    free_if(0)) \
        nocopy(AJ:length(Annz)    alloc_if(0)    free_if(0)) \
        nocopy(Bval:length(Bnnz)    alloc_if(0)    free_if(0)) \
        nocopy(BI:length(An+1)    alloc_if(0)    free_if(0)) \
        nocopy(BJ:length(Bnnz)    alloc_if(0)    free_if(0)) \
        nocopy(CI:    alloc_if(0)    free_if(0)) \
        nocopy(CJ:    alloc_if(0)    free_if(0)) \
        nocopy(Cval:    alloc_if(0)    free_if(0))
    {
        for(i = 0; i < 10; i++) {
            mkl_dcsrmultcsr(&transa, &request, &sort, &Am, &An, &An, Aval, AJ, AI, Bval, BJ, BI, Cval, CJ, CI, &nnzmax, &ierr);
        }
        double s_initial = dsecnd();
        for(i = 0; i < nrepeat; i++) {
            mkl_dcsrmultcsr(&transa, &request, &sort, &Am, &An, &An, Aval, AJ, AI, Bval, BJ, BI, Cval, CJ, CI, &nnzmax, &ierr);
        }
        time_mic_numeric_mm = (dsecnd() - s_initial) / nrepeat; // seconds
    }
    *time = time_mic_numeric_mm;
    if (1) { // Copy result matrix from MIC to Host
    int nelm_CI = Am + 2;
    int nelm_CJ = Cnnz_host + 1;
    int nelm_Cval = Cnnz_host + 1;
    __declspec(target(mic)) MKL_INT* CI_host = (MKL_INT*)mkl_malloc( (nelm_CI) * sizeof( MKL_INT ), 64 );
    __declspec(target(mic)) MKL_INT* CJ_host = (MKL_INT*)mkl_malloc( (nelm_CJ) * sizeof( MKL_INT ), 64 );
    __declspec(target(mic)) double* Cval_host = (double*)mkl_malloc( (nelm_Cval) * sizeof( double ), 64 );
    #pragma offload target(mic:micdev) in(nelm_CI)\
        inout(CI_host:length(nelm_CI)    alloc_if(1)    free_if(0)) \
        inout(CJ_host:length(nelm_CJ)    alloc_if(1)    free_if(0)) \
        inout(Cval_host:length(nelm_Cval)    alloc_if(1)    free_if(0)) \
        nocopy(CI:    alloc_if(0)    free_if(0)) \
        nocopy(CJ:    alloc_if(0)    free_if(0)) \
        nocopy(Cval:    alloc_if(0)    free_if(0))
    {
        int i;
        for(i = 0; i < nelm_CI; i++) CI_host[i] = CI[i];
        for(i = 0; i < nelm_CJ; i++) {CJ_host[i] = CJ[i]; Cval_host[i] = Cval[i];}
    }
    *pCval = Cval_host; *pCJ = CJ_host; *pCI = CI_host;
    }
} /* ENDOF mkl_mic_spmm }}}*/

int main(int argc, char* argv[]) { /*{{{*/
    /** usage */
    int nrequired_args = 7;
    if (argc != nrequired_args){
        fprintf(stderr, "NAME:\n\tmkl_spgemm - multiply two sparse matrices\n");
        fprintf(stderr, "\nSYNOPSIS:\n");
        fprintf(stderr, "\tmkl_spgemm MATRIX_A MATRIX_B MATRIX_C NUMBER_OF_THREADS MIC_ID PRINT_MATRICES\n");
        fprintf(stderr, "\nDESCRIPTION:\n");
        fprintf(stderr, "\tNUMBER_OF_THREADS: {0,1,2,...}\n");
        fprintf(stderr, "\t\t0: Use number of threads determined by MKL\n");
        fprintf(stderr, "\tPRINT_MATRICES: PRINT_YES, PRINT_NO\n");
        fprintf(stderr, "\tMIC_ID: {0,1,2,...}\n");
        fprintf(stderr, "\t\t0: The device ID\n");
        fprintf(stderr, "\nSAMPLE EXECUTION:\n");
        fprintf(stderr, "\texport OFFLOAD_INIT=on_offload;%s test.mtx test.mtx out.mtx 2 0 PRINT_YES\n", argv[0]);
        exit(1);
    }
    /** parse arguments */
    int iarg = 1;
    char* strpathA = argv[iarg];    iarg++;
    char* strpathB = argv[iarg];    iarg++;
    char* strpathC = argv[iarg];    iarg++;
    nthreads = atoi(argv[iarg]);    iarg++;
    if (nthreads > 0) {
        mkl_set_num_threads(nthreads);
    } else {
        nthreads = mkl_get_max_threads();
    }
    micdev = atoi(argv[iarg]);    iarg++;
    option_print_matrices = strcmp(argv[iarg], "PRINT_YES")==0?OPTION_PRINT_MATRICES:OPTION_NOPRINT_MATRICES;    iarg++;
    assert(nrequired_args == iarg);

    /** read matrix market file for A matrix */
    MKL_INT Am, An, Annz;
    MKL_INT *Ax, *Ay;
    double *Anz;
    read_mm(strpathA, &Am, &An, &Annz, &Ax, &Ay, &Anz);

    /** construct csr storage for A matrix */
    MKL_INT* AJ = (MKL_INT*) mkl_malloc( Annz * sizeof( MKL_INT ), 64 );
    MKL_INT* AI = (MKL_INT*) mkl_malloc( (Am+1) * sizeof( MKL_INT ), 64 );
    double* Aval = (double*) mkl_malloc( Annz * sizeof( double ),  64 );
    coo_to_csr(Am, Annz, Ax, Ay, Anz, AI, AJ, Aval);

    /** read matrix market file for B matrix */
    MKL_INT Bm, Bn, Bnnz;
    MKL_INT *Bx, *By;
    double *Bnz;
    read_mm(strpathB, &Bm, &Bn, &Bnnz, &Bx, &By, &Bnz);

    /** construct csr storage for B matrix */
    MKL_INT* BJ = (MKL_INT*) mkl_malloc( Bnnz * sizeof( MKL_INT ), 64 );
    MKL_INT* BI = (MKL_INT*) mkl_malloc( (Bm+1) * sizeof( MKL_INT ), 64 );
    double* Bval = (double*) mkl_malloc( Bnnz * sizeof( double ),  64 );
    coo_to_csr(Bm, Bnnz, Bx, By, Bnz, BI, BJ, Bval);

    /** multiply two matrices */
    double* Cval, time;
    MKL_INT* CJ; MKL_INT* CI;
    mkl_mic_spgemm(Am, An, Annz, Aval, AJ, AI, Bn, Bnnz, Bval, BJ, BI, &Cval, &CJ, &CI, &time);

    int i;
    for(i=0;i<=Am;i++)AI[i]--;    for(i=0;i<Annz;i++)AJ[i]--;
    for(i=0;i<=Bm;i++)BI[i]--;    for(i=0;i<Bnnz;i++)BJ[i]--;
    printmm_one(Am, Cval, CJ, CI);

    /** Write the output C matrix to file */
    printfilemm_one(strpathC, Am, Bn, Cval, CJ, CI);

    /** run my SpGEMM routine in order to find number of multiply-and-add operations */
    long nummult = 0;
    csi* xb = (csi*)calloc(Bn, sizeof(csi));
    csv* x = (csv*)calloc(Bn, sizeof(csv));
    csr* C = csr_multiply(Am, An, Annz, AI, AJ, Aval, Bm, Bn, Bnnz, BI, BJ, Bval, &nummult, xb, x);
    double gflop = 2 * (double) nummult / 1e9;

    /** print gflop per second and time */
    printf("%d\t", nthreads);
    printf("%g\t", (gflop/time));
    printf("%g\t", time);
    printf("%s\t", strpathA);
    printf("%s\t", strpathB);
    printf("%s\t", strpathC);
    printf("\n");

    /** free allocated space */
    mkl_free(AI);
    mkl_free(AJ);
    mkl_free(Aval);
    mkl_free(BI);
    mkl_free(BJ);
    mkl_free(Bval);
    return 0;
} /* ENDOF main }}}*/


