#include "petsc.h"
#include "petscfix.h"
/* ilu.c */
/* Fortran interface file */

/*
* This file was generated automatically by bfort from the C source
* file.  
 */

#ifdef PETSC_USE_POINTER_CONVERSION
#if defined(__cplusplus)
extern "C" { 
#endif 
extern void *PetscToPointer(void*);
extern int PetscFromPointer(void *);
extern void PetscRmPointer(void*);
#if defined(__cplusplus)
} 
#endif 

#else

#define PetscToPointer(a) (*(long *)(a))
#define PetscFromPointer(a) (long)(a)
#define PetscRmPointer(a)
#endif

#include "petscpc.h"
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define pcfactorsetusedroptolerance_ PCFACTORSETUSEDROPTOLERANCE
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define pcfactorsetusedroptolerance_ pcfactorsetusedroptolerance
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define pcfactorsetlevels_ PCFACTORSETLEVELS
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define pcfactorsetlevels_ pcfactorsetlevels
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define pcfactorsetallowdiagonalfill_ PCFACTORSETALLOWDIAGONALFILL
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define pcfactorsetallowdiagonalfill_ pcfactorsetallowdiagonalfill
#endif


/* Definitions of Fortran Wrapper routines */
#if defined(__cplusplus)
extern "C" {
#endif
void PETSC_STDCALL   pcfactorsetusedroptolerance_(PC pc,PetscReal *dt,PetscReal *dtcol,PetscInt *maxrowcount, int *__ierr ){
*__ierr = PCFactorSetUseDropTolerance(
	(PC)PetscToPointer((pc) ),*dt,*dtcol,*maxrowcount);
}
void PETSC_STDCALL   pcfactorsetlevels_(PC pc,PetscInt *levels, int *__ierr ){
*__ierr = PCFactorSetLevels(
	(PC)PetscToPointer((pc) ),*levels);
}
void PETSC_STDCALL   pcfactorsetallowdiagonalfill_(PC pc, int *__ierr ){
*__ierr = PCFactorSetAllowDiagonalFill(
	(PC)PetscToPointer((pc) ));
}
#if defined(__cplusplus)
}
#endif
