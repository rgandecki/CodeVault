#include "Tile.hpp"

#include <iostream>
#include <iterator>
#include <string>

#include <mpi.h>

#include "FileIO.hpp"
#include "State.hpp"

Tile::Tile(const Configuration& cfg, const MpiEnvironment& env)
    : env_(env), cfg_(cfg), header_(FileIO::ReadHeader(cfg.InputFilePath)), //
      tileSize_(FileIO::GetTileInfo(header_.GlobalSize, cfg.Procs, env.worldRank()).Size),
      modelWidth_(tileSize_.Cols + 2) {
	const auto bufsize = (tileSize_.Cols + 2) * (tileSize_.Rows + 2);
	memoryA_.resize(bufsize);
	memoryB_.resize(bufsize);

	model_ = memoryA_.data();
	nextModel_ = memoryB_.data();

	FileIO::Tile(cfg.InputFilePath, header_, cfg.Procs, env_.worldRank(), model_)
	    .Read();
}

Tile Tile::Read(const Configuration& cfg, const MpiEnvironment& env) {
	return {cfg, env};
}

void Tile::write() const {
	const auto& path = cfg_.OutputFilePath;
	FileIO::WriteHeader(header_, path, env_);
	FileIO::Tile(path, header_, cfg_.Procs, env_.worldRank(), model_).Write();
}

std::ostream& operator<<(std::ostream& out, const Tile& t) {
	const auto hline = [](auto& os, auto length) {
		os << '+';
		std::fill_n(std::ostream_iterator<char>(os), length, '-');
		os << "+\n";
	};
	hline(out, t.tileSize().Cols);
	for (std::size_t y{1}; y <= t.tileSize().Rows; ++y) {
		out << '|';
		for (std::size_t x{1}; x <= t.tileSize().Cols; ++x) {
			out << to_integral(t.model()[y * t.modelWidth() + x]);
		}
		out << "|\n";
	}
	hline(out, t.tileSize().Cols);
	return out;
}
