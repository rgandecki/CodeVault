# =================================================================================================
# This file is part of the CodeVault project. The project is licensed under Apache Version 2.0.
# CodeVault is part of the EU-project PRACE-5IP (WP7.3.C).
#
# Author(s):
#   Rafal Gandecki <rafal.gandecki@pwr.edu.pl>
#
# This example demonstrates matrix-matrix multiplication with using numpy library.
# The example takes a triple input arguments (matrix A rows, matrix A cols, matrix B cols),
# specifying the size of the matrices.
#
# =================================================================================================

import sys
import numpy as np


def fill_random(row, cols):
    try:
        return np.random.rand(int(row), int(cols))
    except ValueError:
        print('Arguments should be integer numbers.')
        sys.exit()


def main():
    if len(sys.argv) != 4:
        print('Number of arguments:', len(sys.argv)-1)
        print('Requires exactly 3 argument: matrix_a rows, matrix_a cols and matrix_b cols')
        sys.exit()

    matrix_a = fill_random(sys.argv[1], sys.argv[2])
    matrix_b = fill_random(sys.argv[2], sys.argv[3])

    print(matrix_a)
    print(matrix_b)
    print('-------------------')
    print(np.dot(matrix_a, matrix_b))


if __name__ == "__main__":
    main()
