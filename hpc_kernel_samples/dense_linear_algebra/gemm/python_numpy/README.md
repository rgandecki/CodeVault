README
=======

# 1. Code sample name
gmm_numpy

# 2. Description of the code sample package
This example demonstrates matrix-matrix multiplication with using numpy library.

# 3. Release date
9 May 2018

# 4. Version history
1.0

# 6. Copyright / License of the code sample
Apache Version 2.0

# 5. Contributor (s) / Maintainer(s)
Rafal Gandecki <rafal.gandecki@pwr.edu.pl>

# 6. Language(s)
Python 3.6

# 7. Level of the code sample complexity
basic

# 11. Instructions on how to run the code
- create virtualenv: virtualenv -p python3  path/to/virtualenv/
- activate virtualenv: source path/to/virtualenv/bin/activate
- install dependencies: pip install numpy scipy
- run script

# 12. Sample input(s)
The example takes a triple input arguments numbers of rows and cols matrix A and number of cols matrix B.

# 13. Sample output(s)
Return result of matrix multiplication.
