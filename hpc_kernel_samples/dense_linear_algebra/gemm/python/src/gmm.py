# =================================================================================================
# This file is part of the CodeVault project. The project is licensed under Apache Version 2.0.
# CodeVault is part of the EU-project PRACE-5IP (WP7.3.C).
#
# Author(s):
#   Rafal Gandecki <rafal.gandecki@pwr.edu.pl>
#
# This example demonstrates matrix-matrix multiplication without using numpy library.
# The example is set-up to perform single precision matrix-matrix multiplication.
# The example takes a triple input arguments (matrix A rows, matrix A cols, matrix B cols),
# specifying the size of the matrices.
#
# =================================================================================================


import sys
from random import random


def fill_random(rows, cols):
    return [[random() for _ in range(cols)] for _ in range(rows)]


def main():
    if len(sys.argv) != 4:
        print('Number of arguments:', len(sys.argv)-1)
        print('Requires exactly 3 argument: matrix_a rows, matrix_a cols and matrix_b cols')
        sys.exit()
    try:
        rows_a = int(sys.argv[1])
        cols_a = int(sys.argv[2])
        cols_b = int(sys.argv[3])
    except ValueError:
        print('Arguments should be integer numbers.')
        sys.exit()

    matrix_a = fill_random(rows_a, cols_a)
    matrix_b = fill_random(cols_a, cols_b)

    result_matrix = []

    for i in range(rows_a):
        temp = []
        for j in range(cols_b):
            val = 0
            for k in range(cols_a):
                val += matrix_a[i][k]*matrix_b[k][j]
            temp.append(val)
        result_matrix.append(temp)
    print('Matrix A:{}'.format(matrix_a))
    print('Matrix B: {}'.format(matrix_b))
    print('A * B: {}'.format(result_matrix))


if __name__ == "__main__":
    main()
