# This file is part of the CodeVault project. The project is licensed under Apache Version 2.0.
# CodeVault is part of the EU-project PRACE-5IP (WP7.3.C).
#
# Author(s):
#   Rafal Gandecki <rafal.gandecki@pwr.edu.pl>
#
# This example demonstrates kmeans with using sklearn library.
# The example takes a triple input arguments rows and cols specifying the size of the matrix and numbers of clusters.
#
# =================================================================================================


from sklearn.cluster import KMeans
import numpy as np
import sys


def fill_random(rows, cols):
    return np.random.rand(rows, cols)


def main():
    if len(sys.argv) != 4:
        print('Number of arguments:', len(sys.argv)-1)
        print('Requires exactly 3 arguments: matrix rows, cols and numbers of clusters.')
        sys.exit()

    try:
        rows = int(sys.argv[1])
        cols = int(sys.argv[2])
        clusters = int(sys.argv[3])
    except ValueError:
        print('Error: Arguments should be integer numbers.')
        sys.exit()

    if clusters > rows:
        print('Error: Rows value should be >= numbers of clusters')
        sys.exit()

    matrix = fill_random(rows, cols)
    kmeans = KMeans(n_clusters=clusters, random_state=0).fit(matrix)
    print(kmeans.cluster_centers_)


if __name__ == "__main__":
    main()
