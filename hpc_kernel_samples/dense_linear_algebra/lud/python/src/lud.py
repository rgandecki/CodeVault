# =================================================================================================
# This file is part of the CodeVault project. The project is licensed under Apache Version 2.0.
# CodeVault is part of the EU-project PRACE-4IP (WP7.3.C).
#
# Author(s):
#   Rafal Gandecki <rafal.gandecki@pwr.edu.pl>
#
# This example demonstrates LU decomposition (Doolittle algorithm) without pivoting.
# The example takes a single input argument, specifying the size of the matrices.
#
#
# =================================================================================================

import sys
from random import random


def fill_random(rows, cols):
    return [[random() for _ in range(cols)] for _ in range(rows)]


def empty_array(rows, cols):
    return [[0 for _ in range(cols)] for _ in range(rows)]


def lud(matrix_a):
    n = len(matrix_a)
    L = empty_array(n, n)
    U = empty_array(n, n)
    for i in range(n):
        for j in range(n):
            U[i][j] = matrix_a[i][j]
            for k in range(i):
                U[i][j] -= U[k][j] * L[i][k]
        for j in range(n):
            if j == i:
                L[j][i] = 1
            elif j > i:
                L[j][i] = matrix_a[j][i] / U[i][i]
                for k in range(i):
                    L[j][i] -= ((U[k][i] * L[j][k]) / U[i][i])
    return L, U


def main():
    if len(sys.argv) != 2:
        print('Number of arguments:', len(sys.argv)-1)
        print('Requires exactly 1 argument: matrix length')
        sys.exit()
    try:
        n = int(sys.argv[1])
    except ValueError:
        print('Argument should be integer number.')
        sys.exit()

    matrix_a = fill_random(n, n)
    L, U = lud(matrix_a)

    print("A:")
    print(matrix_a)

    print("L:")
    print(L)

    print("U:")
    print(U)


if __name__ == "__main__":
    main()
