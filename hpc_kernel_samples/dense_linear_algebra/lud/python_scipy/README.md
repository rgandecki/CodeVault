README
=======

# 1. Code sample name
lud_scipy

# 2. Description of the code sample package
This example demonstrates LU decomposition (Doolittle algorithm) with pivoting.

# 3. Release date
8 May 2018

# 4. Version history
1.0

# 6. Copyright / License of the code sample
Apache Version 2.0

# 5. Contributor (s) / Maintainer(s)
Rafal Gandecki <rafal.gandecki@pwr.edu.pl>

# 6. Language(s)
Python 3.6

# 7. Level of the code sample complexity
basic

# 11. Instructions on how to run the code
- create virtualenv: virtualenv -p python3  path/to/virtualenv/
- activate virtualenv: source path/to/virtualenv/bin/activate
- install dependencies: pip install numpy scipy
- run script

# 12. Sample input(s)
The example takes a single input argument, specifying the size of the matrices.

# 13. Sample output(s)
Return matrices L and U.
