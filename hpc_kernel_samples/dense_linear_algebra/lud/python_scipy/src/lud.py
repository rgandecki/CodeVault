# =================================================================================================
# This file is part of the CodeVault project. The project is licensed under Apache Version 2.0.
# CodeVault is part of the EU-project PRACE-5IP (WP7.3.C).
#
# Author(s):
#   Rafal Gandecki <rafal.gandecki@pwr.edu.pl>
#
# This example demonstrates LU decomposition with using SciPy library.
# Compute pivoted LU decompostion of a matrix.
# The example takes a single input argument, specifying the size of the matrices.
#
# =================================================================================================

import sys
import numpy as np
import scipy.linalg


def fill_random(row):
    try:
        return np.random.rand(int(row), int(row))
    except ValueError:
        print('Argument should be  integer number.')
        sys.exit()


def main():
    if len(sys.argv) != 2:
        print('Number of arguments:', len(sys.argv)-1)
        print('Requires exactly 1 argument: matrix length')
        sys.exit()

    matrix_a = fill_random(sys.argv[1])
    P, L, U = scipy.linalg.lu(matrix_a)

    print("A:")
    print(matrix_a)

    print("P:")
    print(P)

    print("L:")
    print(L)

    print("U:")
    print(U)


if __name__ == "__main__":
    main()
