# Packages are optional: if they are not present, certain code samples are not compiled
cmake_minimum_required(VERSION 2.8.10 FATAL_ERROR)

find_package(MPI)      # Built-in in CMake
find_package(Boost 1.55)

include(${CMAKE_CURRENT_SOURCE_DIR}/../../../cmake/common.cmake)

# ==================================================================================================

if ("${DWARF_PREFIX}" STREQUAL "")
  set(DWARF_PREFIX 4_nbody)
endif()

set(NAME ${DWARF_PREFIX}_naive__shared_mem)


if (MPI_FOUND AND Boost_FOUND)
	enable_language(CXX)
    include_directories(${MPI_INCLUDE_PATH} ${Boost_INCLUDE_DIR})
    add_executable(${NAME} main.cpp)
    set(CMAKE_BUILD_TYPE RelWithDebInfo)
    if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
    	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -march=native -Wall -Wextra")
	elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
    	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -march=native -Weverything -Wno-missing-prototypes -Wno-padded -Wno-c++98-compat -Wno-c++98-compat-pedantic")
    elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Intel")
    	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -xHost -std=c++14 -Wall")
    endif()
    set_target_properties(${NAME} PROPERTIES CXX_STANDARD 14 CXX_STANDARD_REQUIRED YES)
    target_link_libraries(${NAME} ${MPI_LIBRARIES} ${Boost_LIBRARIES} )
    install(TARGETS ${NAME} DESTINATION bin)
    message("** Enabling '${NAME}': with MPI and Boost")
else()
    message("## Skipping '${NAME}': MPI or Boost support missing")
#    dummy_install(${NAME} "MPI")
endif()

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${C_FLAGS}")

unset(NAME)
# ==================================================================================================
