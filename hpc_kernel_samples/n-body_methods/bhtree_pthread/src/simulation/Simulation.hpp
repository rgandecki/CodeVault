#ifndef SIMULATION_HPP
#define SIMULATION_HPP

#include <vector>
#include <Body.hpp>
#include <BarnesHutTree.hpp>

namespace nbody {
	using namespace std;

	//simulation superclass
	class Simulation {
	protected:
		int parallelSize;
		int parallelRank;
		int correctState;
		vector<Body> bodies;
		BarnesHutTree* tree;
	public:
		Simulation();
		virtual ~Simulation();
		virtual void clearBodies();
		virtual void addBodies(vector<Body> bodies);
		virtual vector<Body> getBodies();
		virtual int getNumberOfProcesses() = 0;
		virtual int getProcessId() = 0;
		virtual bool readInputData(string filename);
		virtual Tree* getTree();
	};
}

#endif
