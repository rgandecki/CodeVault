#ifndef PTHREAD_SIMULATION_HPP
#define PTHREAD_SIMULATION_HPP

#include "Simulation.hpp"

namespace nbody {
	class PthreadSimulation : public Simulation {
		friend class BarnesHutTreeThreaded;
	protected:
		int iterations;
		pthread_barrier_t barrier;
		vector<pthread_t> threads;

		static void* run(void* data);
		static int getThreadId(pthread_t thread, vector<pthread_t> threads);
	public:
		PthreadSimulation(string inputFile);
		virtual ~PthreadSimulation();
		virtual int getNumberOfProcesses();
		virtual int getProcessId();
		virtual void setIterations(int iterations);
		virtual void run();
	};
}

#endif
